# coding: utf-8
import re
from odoo import fields, models, api


class archiModel(models.Model):

    _name = 'archi.model'

    name = fields.Char(string="Archimate model")
    archimate_model_file = fields.Binary(string="Archimate model file",help="model file with extension .archimate")
    model_file_name = fields.Char()
    archimate_model_html = fields.Binary(string="Archimate model export to html", help="zip file (export to html folders)")
    images_file_name = fields.Char()
    archimate_model_id = fields.Char(string="Model id")
    # menu ids
    archi_menu_ids = fields.Many2many('archi.model.menu', string="Menu")
    # elements
    elements = fields.Many2many('archi.element', string="Elements")
    folder_types = fields.Many2many('archi.folder.types', string="Folder types")
    element_types = fields.Many2many('archi.element.types', string="Element types")
    properties_list = fields.Many2many('properties.list',string="Properties in model")


    @api.onchange('archimate_model_file')
    def change_name(self):
        file_name = self.model_file_name
        if file_name:
            self.name = file_name.replace('.archimate','')



    

class archiModelMenu(models.Model):
    
    _name = 'archi.model.menu'
    _description = 'Folders for lateral menu, every folder connected to folder or list of element'
    
    model_id = fields.Many2one('archi.model', string="Model",  ondelete="cascade")
    parent_folder = fields.Many2one('archi.model.menu', string="Parent item")
    name = fields.Char(string="Item name")
    identifier = fields.Char(string="Identifier")
    folder_type = fields.Many2one('archi.folder.types', string="Folder type")


    def check_existing(self, identifier, model_id):

        """ check if record with this identifier exist or not """

        get_by_identifier = self.env['archi.model.menu'].search([('identifier','=',identifier),
                                                                 ('model_id','=',model_id)],limit=1)
        if get_by_identifier:
            return get_by_identifier
        else:
            return get_by_identifier

    @api.model
    def create(self, values):

        """ override create method for updating if record with identifier exist """

        if values.get('identifier') and values.get('model_id'):
            get_menu_instance = self.check_existing(values.get('identifier'),values.get('model_id'))
            if get_menu_instance:
                get_menu_instance.write(values)
                return
        return super(archiModelMenu, self).create(values)



class archiElement(models.Model):

    _name = 'archi.element'
    
    model_id = fields.Many2one('archi.model', string="Model",  ondelete="cascade")
    name = fields.Char(string="Name")
    parent_folder = fields.Many2one('archi.model.menu',string="Parent folder")
    target = fields.Char(string="target")
    source = fields.Char(string="source")
    element_identifier = fields.Char(string="Element ID")
    element_type = fields.Many2one('archi.element.types', string="Element type")
    child_ids = fields.One2many('archi.child', 'parent_id', string="Childs")
    icon_path = fields.Char(compute="_get_icon_path")
    diagrama = fields.Binary(string="Diagrama")
    property_ids = fields.One2many('element.properties.list','element_id',string="Properties")
    documentation = fields.Text(string="Documents")


    def _get_icon_path(self):
        for item in self:
            element_type = item.element_type.name
            if element_type:
                item.icon_path = '/archi_web/static/icons/'+element_type.lower().replace(' ','-')+'.png'
    

    def check_existing(self, identifier, model_id):
    
        """ check if record with this identifier exist or not """

        get_by_identifier = self.env['archi.element'].search([('element_identifier','=',identifier),
                                                                 ('model_id','=',model_id)],limit=1)
        if get_by_identifier:
            return get_by_identifier
        else:
            return get_by_identifier

    @api.model
    def create(self, values):
        if values.get('element_identifier') and values.get('model_id'):
            get_menu_instance = self.check_existing(values.get('element_identifier'),values.get('model_id'))
            if get_menu_instance:
                get_menu_instance.write(values)
                return
        return super(archiElement, self).create(values)


class elementPropertiesList(models.Model):
    
    _name = 'element.properties.list'

    element_id = fields.Many2one('archi.element',string="Element", ondelete="cascade")
    name = fields.Many2one('properties.list', string="Name")
    value = fields.Char(string="Value")

class propertiesList(models.Model):

    _name = 'properties.list'
    
    model_id = fields.Many2one('archi.model', string="Model",  ondelete="cascade")
    name = fields.Char(string="Property name")
    property_type = fields.Selection([('number','number'),('string','string')], string="type of property")


class archiChild(models.Model):

    _name = 'archi.child'
    _rec_name = 'child_id'
    

    model_id = fields.Many2one('archi.model',string="Archi model", ondelete="cascade")
    child_type = fields.Char(string="Child Type")
    child_id = fields.Char(string="Child ID")
    parent_id = fields.Many2one('archi.element')
    archimate_element_id = fields.Many2one('archi.element', string="Element")
    element_identifier = fields.Char(related='archimate_element_id.element_identifier',store=True)
    bounds_x = fields.Integer(string="bounds x1")
    bounds_y = fields.Integer(string="bounds y1")
    bounds_x1 = fields.Integer(string="bounds x2")
    bounds_y1 = fields.Integer(string="bounds y2")
    bounds_width = fields.Integer(string="bounds width")
    bounds_height = fields.Integer(string="bounds height")


    def check_existing(self, identifier, model_id):
        
        """ check if record with this identifier exist or not """

        get_by_identifier = self.env['archi.child'].search([('child_id','=',identifier),
                                                            ('model_id','=',model_id)],limit=1)
        if get_by_identifier:
            return get_by_identifier
        else:
            return get_by_identifier

    @api.model
    def create(self, values):
        if values.get('child_id') and values.get('model_id'):
            get_menu_instance = self.check_existing(values.get('child_id'),values.get('model_id'))
            if get_menu_instance:
                get_menu_instance.write(values)
                return
        return super(archiChild, self).create(values)


class archiFolderTypes(models.Model):

    _name = 'archi.folder.types'

    name = fields.Char(string="Type")


class archiElementTypes(models.Model):

    _name = 'archi.element.types'

    name = fields.Char(string="Type")


