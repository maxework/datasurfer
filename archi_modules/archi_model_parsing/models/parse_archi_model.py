# coding: utf-8
import base64
import zipfile
import io
import re
from xml.etree import ElementTree
from odoo import fields, models, api
from bs4 import BeautifulSoup
import logging

_logger = logging.getLogger(__name__)

ns = {
            "xsi":"{http://www.w3.org/2001/XMLSchema-instance}",
            "archimate":"{http://www.archimatetool.com/archimate}"
        }

class archiModelParsing(models.Model):

    _inherit = 'archi.model'



    def extract_from_zip(self):

        def get_element_identifier_image(zip_namelist):
            for item in zip_namelist:
                path_dict = item.split('/')
                try:
                    if path_dict[1:][1] == 'images' and path_dict[1:][-1] != '':
                        element_identifier = path_dict[-1].split('.')[0]
                        _logger.debug('element identifier: %s',element_identifier)
                        file_write = zip_file.read(item)
                        get_current_element = self.env['archi.element'].search(
                            [('element_identifier', '=', element_identifier)])
                        get_current_element.write({
                            'diagrama': base64.b64encode(file_write)
                        })
                except:
                    pass

        def get_element_diagramma_coords(zip_namelist):

            for item_path in zip_file.namelist():
                path_dict = item_path.split('/')
                # print(path_dict)
                #if path_dict[1].find('id-') != -1:
                try:
                    if path_dict[1:][1] == 'views' and path_dict[-1] != '':
                        html_text = zip_file.read(item_path)
                        diagrama_id = path_dict[-1].split('.')[0]
                        get_diagram_id = self.env['archi.element']\
                            .search([('element_identifier', '=', diagrama_id)])
                        soup = BeautifulSoup(html_text, 'lxml')
                        areas_list = soup.findAll('area')
                        for item in areas_list:
                            get_child = self.env['archi.child'].search([('parent_id', '=' ,get_diagram_id.id),
                                                                        ('element_identifier', '=', item.attrs.get('href').split('/')[-1].split('.')[0] )])
                            coords_list = item.attrs.get('coords').split(',')
                            get_child.write({
                                'bounds_x': coords_list[0],
                                'bounds_y': coords_list[1],
                                'bounds_x1': coords_list[2],
                                'bounds_y1': coords_list[3]
                            })
                            _logger.debug('Coordinater %s',item.attrs.get('coords'))
                            _logger.debug('href: %s',item.attrs.get('href').split('/')[2].split('.')[0])
                except:
                    pass



        if self.archimate_model_html:
            zip_file_extract = base64.b64decode(self.archimate_model_html)
            content = io.BytesIO(zip_file_extract)
            zip_file = zipfile.ZipFile(content)
            zip_namelist = zip_file.namelist()
            get_element_identifier_image(zip_namelist)
            get_element_diagramma_coords(zip_namelist)
            # for item in zip_file.namelist():
            #     file_write = zip_file.read(item)
            #     get_id = item.split('/')
            #     get_current_element = self.env['archi.element'].search([('element_identifier','=',get_id[1].split('.')[0])])
            #     get_current_element.write({
            #         'diagrama':base64.b64encode(file_write)
            #     })


    
    def get_folder_types(self, root_xml):
        item_folder_list = []
        for item_folder in root_xml.iter():
            if item_folder.get('type') is not None and item_folder.tag == 'folder':
                if not self.env['archi.folder.types'].search([('name','=',item_folder.get('type'))]):
                    item_folder_list.append(item_folder.get('type'))
        for write_folder in list(set(item_folder_list)):
            if not self.env['archi.folder.types'].search([('name','=',write_folder)]):
                folder_res = self.env['archi.folder.types'].create({
                    'name':write_folder
                })
            
    
    def get_element_types(self, root_xml):
        item_element_list = []
        for item_element in root_xml.iter():
            if item_element.get(ns['xsi']+'type') is not None and item_element.tag == 'element':
                if not self.env['archi.element.types'].search([('name','=',item_element.get(ns['xsi']+'type'))]):
                    item_element_list.append(item_element.get(ns['xsi']+'type').split(':')[1])
        for write_element in list(set(item_element_list)):
            if not self.env['archi.element.types'].search([('name','=',write_element)]):
                element_res = self.env['archi.element.types'].create({
                    'name':write_element
                })
            
    def get_properties(self, root_xml):
        all_properties_list = []
        full_properties_list = []
        name_properties_list = []
        for item_property in root_xml.iter():
            if item_property.tag == 'property':
                _logger.debug('Property import: %s', item_property.attrib)
                try:
                    full_properties_list.append({'key':item_property.attrib.get('key'),
                                                 'value':item_property.attrib.get('value')})
                except:
                    _logger.debug('Exception: %s, %s',item_property.attrib, item_property)
        
        for item_properties_list in full_properties_list:
            if item_properties_list.get('key') not in name_properties_list:
                all_properties_list.append(item_properties_list)
                name_properties_list.append(item_properties_list.get('key'))
        # create properties and check if it exist do not create it
        get_all_properties_from_db = self.env['properties.list'].search([('name','in',name_properties_list)]).mapped('name') or []
        
        for item_property_create in all_properties_list:
            if item_property_create:
                create_value = item_property_create.get('value')
            else:
                create_value = ''
            if create_value is None:
                create_value = ''
            _logger.debug('Properties get value: %s', item_property_create)
            if bool(re.sub('[^0-9.]','',create_value)):
                property_type = 'number'
            else:
                property_type = 'string'
            
            if item_property_create.get('key') not in get_all_properties_from_db:
                res_property = self.env['properties.list'].create({
                    'name':item_property_create.get('key'),
                    'model_id':self.id,
                    'property_type':property_type
                })
            else:
                get_property = self.env['properties.list'].search([('name','=',item_property_create.get('key'))])
                if get_property:
                    get_property.write({
                                        'name':item_property_create.get('key'),
                                        'model_id':self.id,
                                        'property_type':property_type
                                        })

        properties_key_id_dict = {}
        properties_list_key_id = self.env['properties.list'].search([('name','in',name_properties_list)]) or []
        for item_property in properties_list_key_id:
            if item_property.name not in properties_key_id_dict.keys():
                properties_key_id_dict.update({
                   item_property.name:item_property.id
                })
        return properties_key_id_dict

    

    
    def create_folder(self, parent_folder, current_folder):
        folder_write_create_dict = {
                                    "model_id":self.id,
                                    "name":current_folder.get('name'),
                                    "identifier":current_folder.get('id'),
                                    "folder_type": self.env['archi.folder.types'].search([('name','=',current_folder.get('type'))]).id
                                    }
        if parent_folder is not None:
            current_parent_folder = self.env['archi.model.menu'].search([('identifier','=',parent_folder.attrib.get('id'))])
            if current_parent_folder:
                folder_write_create_dict.update({'parent_folder':current_parent_folder.id})
        if not self.env['archi.model.menu'].search([('identifier','=',current_folder.get('id'))]):
            folder_res = self.env['archi.model.menu'].create(folder_write_create_dict)
        else:
            current_folder = self.env['archi.model.menu'].search([('identifier','=',current_folder.get('id'))]) 
            current_folder.write(folder_write_create_dict)
    

    def create_element(self, parent_folder, element_object):

        """ create element and add parent forlder for this element """
        element_write_dict = {}
        get_current_parent_folder = self.env['archi.model.menu'].search([('identifier','=',parent_folder.attrib.get('id'))])

        if element_object.get('name') is not None:
            element_write_dict.update({'name':element_object.get('name')})
        else:
            element_write_dict.update({'name':element_object.get(ns['xsi']+'type').split(':')[1]})
        if element_object.get('target') is not None:
            element_write_dict.update({'target':element_object.get('target')})
        if element_object.get('source') is not None:
            element_write_dict.update({'source':element_object.get('source')})
        if element_object.get('id') is not None:
            element_write_dict.update({'element_identifier':element_object.get('id')})

        if element_object.get(ns['xsi']+'type') is not None:
            element_type_id = self.env['archi.element.types'].search([('name','=',element_object.get(ns['xsi']+'type').split(':')[1])])
            element_write_dict.update({'element_type':element_type_id.id})

        documentation = element_object.find('documentation')
        if documentation is not None:
            element_write_dict.update({'documentation':element_object.find('documentation').text})
        element_write_dict.update({'parent_folder':get_current_parent_folder.id,"model_id":self.id})
        
        current_element = self.env['archi.element'].search([('element_identifier','=',element_write_dict['element_identifier'])])
        if not current_element:
            res_element = self.env['archi.element'].create(element_write_dict)
        else:
            current_element.write(element_write_dict)

    def create_child(self,item_element, item_child):
        _logger.debug('TEST ELEMENT CREATE CHILD: %s %s',item_element.attrib.get('name'),item_element.attrib.get('id'))
        archimate_element_id = self.env['archi.element'].search([('element_identifier','=',item_element.attrib.get('id'))]).id
        child_bound = item_child.find('bounds')
        #get element
        if item_child.get('archimateElement'):
            archimateElement = item_child.get('archimateElement')
        elif item_child.get('model'):
            archimateElement = item_child.get('model')
        else:
            archimateElement = False

        archimate_element_ref = self.env['archi.element'].search([('element_identifier', '=', archimateElement)]).id
        res_child_dict = {  "model_id":self.id, 
                            "child_type":item_child.get(ns['xsi']+'type'),
                            "child_id":item_child.get('id'),
                            "archimate_element_id":archimate_element_ref,
                            "bounds_x":child_bound.get('x'),
                            "bounds_y":child_bound.get('y'),
                            "bounds_width":child_bound.get('width'),
                            "bounds_height":child_bound.get('height')
                         }
        current_child = self.env['archi.child'].search([('child_id','=',item_child.get('id'))])
        if current_child:
            current_child.write(res_child_dict)
            return current_child.id
        else:
            res_current_child = self.env['archi.child'].create(res_child_dict)
            return res_current_child.id 
        



    def getModelsFromXML(self):
        """ get elements with child and menu from file and get images from zip file """
        xml_file = base64.b64decode(self.archimate_model_file)
        xml_string = xml_file.decode()
        root_xml = ElementTree.fromstring(xml_string)
        
        
        # get all folder's type
        self.get_folder_types(root_xml)
        
        # get all elements types
        self.get_element_types(root_xml)  

        # get properties with dict like {'property name':id of property}
        property_key_id_dict = self.get_properties(root_xml)


        def get_properties(item_element, property_key_id_dict):
            _logger.debug('Properties elements: %s %s',item_element,property_key_id_dict)
            property_ids = [] 
            for item_property in item_element:
                if item_property.tag == 'property':
                    property_dict = {'value':item_property.get('value'),'name':property_key_id_dict.get(item_property.get('key'))}
                    property_ids.append((0,0,property_dict))
            current_element = self.env['archi.element'].search([('element_identifier','=',item_element.get('id'))])
            if current_element.property_ids.ids == []:
                current_element.write({'property_ids':property_ids})
            else:
                delete_ids = []
                for item_for_delete in current_element.property_ids.ids:
                    delete_ids.append((2,item_for_delete))
                current_element.write({'property_ids':delete_ids})
                if current_element.property_ids.ids == []:
                    current_element.write({'property_ids':property_ids})
            

        def get_childs(item_element):
            childs_list = []
            childs_to_end = []
            child_temp = False

            def get_childs_tree(parent_child):
                if bool(parent_child.findall('child')):
                    for item_child_tree in parent_child.findall('child'):
                        if bool(item_child_tree.findall('child')):
                            get_childs_tree(item_child_tree)
                        else:
                            childs_list.append(self.create_child(item_element, item_child_tree))
                    childs_list.append(self.create_child(item_element,parent_child))


            for child in item_element.findall('child'):

                if bool(child.findall('child')):
                    _logger.debug('TEST CHILD COND: %s', child.findall('child'))
                    get_childs_tree(child)
                else:
                    _logger.debug("Attribute type %s",child.attrib.get('type'))
                    child_id = self.create_child(item_element, child)
                    childs_list.append(child_id)
            _logger.debug('TEST CHILD LIST %s',childs_list)
            current_element = self.env['archi.element'].search([('element_identifier','=',item_element.get('id'))])
            current_element.write({'child_ids':[(6,0,childs_list)]})

        def get_elements(folder_element):
            for element_item in folder_element:
                if element_item.tag == 'element':
                    self.create_element(folder_element,element_item)
                    if element_item.find('child') is not None:
                        get_childs(element_item)
                    if element_item.find('property') is not None:
                        get_properties(element_item, property_key_id_dict)

        
        
        def get_folders(folder_item):
            for folder in folder_item:
                if folder.tag == 'folder':
                    self.create_folder(folder_item, folder)
                    get_elements(folder)
                    
                if folder.find('folder') is not None:
                    get_folders(folder)

        for item in root_xml:
            if item.tag == 'folder':
                self.create_folder(None,item)
                if item.find('folder') is not None:
                    get_elements(item)
                    get_folders(item)
                else:
                    get_elements(item)



        folder_types_ids = self.env['archi.folder.types'].search([]).mapped('id') 
        element_types_ids = self.env['archi.element.types'].search([]).mapped('id')
        element_ids = self.env['archi.element'].search([('model_id','=',self.id)]).mapped('id')
        menu_ids = self.env['archi.model.menu'].search([('model_id','=',self.id)]).mapped('id')
        properties_list = self.env['properties.list'].search([('model_id','=',self.id)]).mapped('id')
        # # write to model
        self.write({
            'folder_types':[(6,0,folder_types_ids)],
            'element_types':[(6,0,element_types_ids)],
            'elements':[(6,0,element_ids)],
            'archi_menu_ids':[(6,0,menu_ids)],
            'properties_list':properties_list
        })







        # for element_object in root_xml.iter():
        #     if element_object.tag == 'element':
        #         element_write_dict = {}
                
                    

        #         if element_object.get('name') is not None:
        #             element_write_dict.update({'name':element_object.get('name')})
        #         else:
        #             element_write_dict.update({'name':element_object.get(ns['xsi']+'type').split(':')[1]})
        #         if element_object.get('target') is not None:
        #             element_write_dict.update({'target':element_object.get('target')})
        #         if element_object.get('source') is not None:
        #             element_write_dict.update({'source':element_object.get('source')})
        #         if element_object.get('id') is not None:
        #             element_write_dict.update({'element_identifier':element_object.get('id')})
        #         if element_object.get(ns['xsi']+'type') is not None:
        #             element_type_id = self.env['archi.element.types'].search([('name','=',element_object.get(ns['xsi']+'type').split(':')[1])])
        #             element_write_dict.update({'element_type':element_type_id.id})
                
        #         if element_object.findall('child') is not None:
        #             element_childs_list = []

        #             for item_child in element_object.findall('child'):
        #                 get_child = self.env['archi.child'].search([('child_id','=', item_child.get('id'))])
        #                 if not get_child:
        #                     child_bound = item_child.find('bounds')
        #                     archimate_element_id = self.env['archi.element']\
        #                         .search([('element_identifier','=',item_child.get('archimateElement'))])
        #                     print(archimate_element_id)
        #                     res_child = self.env['archi.child'].create({
        #                         "child_type":item_child.get(ns['xsi']+'type'),
        #                         "child_id":item_child.get('id'),
        #                         "archimate_element_id":archimate_element_id.id,
        #                         "bounds_x":child_bound.get('x'),
        #                         "bounds_y":child_bound.get('y'),
        #                         "bounds_width":child_bound.get('width'),
        #                         "bounds_height":child_bound.get('height')
        #                     })
        #                     element_childs_list.append(res_child.id)
        #                     if item_child.findall('child'):
        #                         for next_child in item_child.findall('child'):
        #                             archimate_next_element_id = self.env['archi.element']\
        #                                 .search([('element_identifier','=',next_child.get('archimateElement'))])
        #                             if not self.env['archi.child'].search([('child_id','=', next_child.get('id'))]):
        #                                 next_child_bound = next_child.find('bounds')
        #                                 res_next_child = self.env['archi.child'].create({
        #                                     "child_type":next_child.get(ns['xsi']+'type'),
        #                                     "parent_id":res_child.id,
        #                                     "child_id":next_child.get('id'),
        #                                     "archimate_element_id":archimate_next_element_id.id,
        #                                     "bounds_x":next_child.get('x'),
        #                                     "bounds_y":next_child.get('y'),
        #                                     "bounds_width":next_child.get('width'),
        #                                     "bounds_height":next_child.get('height')
        #                                 })
        #                                 element_childs_list.append(res_next_child.id)
        #                 else:
        #                     element_childs_list.append(get_child.id)
        #         # if element_object.findall('child'):
        #         #     import pdb; pdb.set_trace()
        #         element_write_dict.update({"child_ids":[(6,0,element_childs_list)]})
        #         current_element = self.env['archi.element'].search([('element_identifier','=',element_write_dict['element_identifier'])])
        #         if not current_element:
        #             res_element = self.env['archi.element'].create(element_write_dict)
        #             #add properties to element
        #             if element_object.findall('property') is not None:
        #                 create_property_list = []
        #                 for item_property in element_object.findall('property'):
        #                 # check record
        #                     create_property_list.append((0,0,{'name':properties_key_id_dict.get(item_property.attrib['key']),
        #                                                       'value':item_property.attrib.get('value')}))
        #                 res_element.write({'property_ids':create_property_list})
        #         else:
        #             current_element.write(element_write_dict)

        # # unzip file and extract images and connect this images to element
        # # extract file


        # # get folders and elements to folder and write to 'archi.model.menu'
        # for item in root_xml.findall('folder'):

        #     def create_folder(folder_item):
        #         folder_write_create_dict = {
        #                                 "name":folder_item.get('name'),
        #                                 "identifier":folder_item.get('id'),
        #                                 "folder_type": self.env['archi.folder.types'].search([('name','=',folder_item.get('type'))]).id
        #                             }
        #         if not self.env['archi.model.menu'].search([('identifier','=',folder_item.get('id'))]):
        #             folder_res = self.env['archi.model.menu'].create(folder_write_create_dict)
        #         else:
        #             current_folder = self.env['archi.model.menu'].search([('identifier','=',folder_item.get('id'))]) 
        #             current_folder.write(folder_write_create_dict)


        #     def inside_folder(folder_xml):
        #         if folder_xml.findall('folder') is not None:
        #             for item_folder in folder_xml.findall('folder'):
        #                 if not self.env['archi.model.menu'].search([('identifier','=',item_folder.get('id'))]):
        #                     res_folder = self.env['archi.model.menu'].create({
        #                         "parent_id": self.env['archi.model.menu'].search([('identifier','=',folder_xml.get('id'))]).id or False,
        #                         "name":item_folder.get('name'),
        #                         "identifier":item_folder.get('id'),
        #                         "folder_type": self.env['archi.folder.types'].search([('name','=',item_folder.get('type'))]).id,
        #                     })
        #                     get_folder_element(item_folder)
        #                 else:
        #                     self.env['archi.model.menu'].write({
        #                         "parent_id": self.env['archi.model.menu'].search([('identifier','=',folder_xml.get('id'))]).id or False,
        #                         "name":item_folder.get('name'),
        #                         "identifier":item_folder.get('id'),
        #                         "folder_type": self.env['archi.folder.types'].search([('name','=',item_folder.get('type'))]).id,
        #                     })
        #                     get_folder_element(item_folder)
        #         else:
        #             return  

        #     def get_folder_element(folder_item):
        #         if folder_item.get('type') == 'relations':
        #             for item_relation in folder_item.findall('element'):
        #                 source_name = self.env['archi.element'].search([('element_identifier','=',item_relation.get('source'))]).name
        #                 target_name = self.env['archi.element'].search([('element_identifier','=',item_relation.get('target'))]).name
        #                 create_write_dict = {
        #                         "parent_id": self.env['archi.model.menu'].search([('identifier','=',folder_item.get('id'))]).id or False,
        #                         "name":item_relation.get(ns['xsi']+'type').split(':')[1]+' ('+source_name+'-'+target_name+')',
        #                         "identifier":item_relation.get('id'),
        #                         "folder_element_type": self.env['archi.element.types'].search([('name','=',item_relation.get(ns['xsi']+'type').split(':')[1])]).id,
        #                         "element_id": self.env['archi.element'].search([('element_identifier','=',item_relation.get('id'))]).id
        #                     }
        #                 if not self.env['archi.model.menu'].search([('identifier','=',item_relation.get('id'))]):
        #                     res_folder = self.env['archi.model.menu'].create(create_write_dict)
        #                 else:
        #                     current_element = self.env['archi.model.menu'].search([('identifier','=',item_relation.get('id'))])
        #                     current_element.write(create_write_dict)
        #         if folder_item.findall('element') is not None and folder_item.get('type') != 'relations':
        #             for item_element in folder_item.findall('element'):
        #                 create_update_dict = {
        #                         "parent_id": self.env['archi.model.menu'].search([('identifier','=',folder_item.get('id'))]).id or False,
        #                         "name":item_element.get('name'),
        #                         "identifier":item_element.get('id'),
        #                         "folder_element_type": self.env['archi.element.types'].search([('name','=',item_element.get(ns['xsi']+'type').split(':')[1])]).id,
        #                         "element_id": self.env['archi.element'].search([('element_identifier','=',item_element.get('id'))]).id
        #                     }
        #                 if not self.env['archi.model.menu'].search([('identifier','=',item_element.get('id'))]):
        #                     res_folder = self.env['archi.model.menu'].create(create_update_dict)
        #                 else:
        #                     current_element = self.env['archi.model.menu'].search([('identifier','=',item_element.get('id'))])
        #                     current_element.write(create_update_dict)
        #                 #self.env.cr.commit()
        #     create_folder(item)
        #     get_folder_element(item)
        #     inside_folder(item)
        # # write to model folders elements
        # # get list of ids 
        # folder_types_ids = self.env['archi.folder.types'].search([]).mapped('id') 
        # element_types_ids = self.env['archi.element.types'].search([]).mapped('id')
        # element_ids = self.env['archi.element'].search([]).mapped('id')
        # menu_ids = self.env['archi.model.menu'].search([]).mapped('id')
        # # write to model
        # self.write({
        #     'folder_types':[(6,0,folder_types_ids)],
        #     'element_types':[(6,0,element_types_ids)],
        #     'elements':[(6,0,element_ids)],
        #     'archi_menu_ids':[(6,0,menu_ids)]
        # })

        # return True
        





