{
    'name': "Archi model parsing",
    'version': '1.1',
    'depends': [
        'base',
        'bus',
        'web'
    ],
    'external_dependencies': {
        'binary': ['unoconv']
    },
    'author': "Stepanets Sergey",
    'category': 'Application',
    'description': """
       Module models of archi 
    """,
    'data': [
       'security/ir.model.access.csv',
       'views/model_exchange_file/model_exchange.xml',
       'views/menu_items.xml'
    ],
    'images': [
        'static/description/configuration.png',
    ],
}