odoo.define('archi_web.archi_models', function (require) {
    'use strict';
    
    var Class = require('web.Class');
    var rpc = require('web.rpc');
    
    var archiModel = Class.extend({
        init: function (values) {
            Object.assign(this, values);
            this.menu_list = [];
            this.diagrama = '';
            this.visualize = {nodes:[],edges:[]};
            this.models = [];
            this.statistic_filters = [];
            this.statistic_chart = {};

        },


        getStatisticsChartDataById: function(model_id, property_id) {
            var self = this;
            return rpc.query({
                model: 'archi.model',
                method: 'getStatisticsChartDataById',
                args: [[self.id]],
                kwargs: {'model_id':model_id, 'property_id':property_id}
            }).then( function(statistic_chart) {
               self.statistic_chart = statistic_chart;
               return self;
            });
        },

        getStatisticTable: function(model_id, properties_list) {
            var self = this;
            return rpc.query({
                model: 'archi.model',
                method: 'getStatisticTable',
                args: [[self.id]],
                kwargs: {'model_id':model_id, 'properties_list':properties_list}
            }).then( function(statistic_table) {
               self.statistic_table = statistic_table;
               return self;
            });
        },
        getPropertiesByElementID: function(element_id) {
           var self = this;
           return rpc.query({
             model: 'archi.model',
             method: 'get_properties_by_element_id',
             args: [[self.id]],
             kwargs: {'element_id':element_id}
           }
           ).then(function(result_properties) {
             return result_properties;
           })
        },

        getStatisticsFilters: function(model_id) {
            var self = this;
            return rpc.query({
                model: 'archi.model',
                method: 'getStatisticsFilters',
                args: [[self.id]],
                kwargs: {'model_id':model_id}
            }).then( function(statistic_filters) {
               self.statistic_filters = statistic_filters;
               return self;
            });

        },
        getArchimateModels: function() {
           var self = this;
           return rpc.query({
               model: 'archi.model',
               method: 'getArchimateModels',
               args: [[self.id]],
               kwargs: {}
           }).then( function(models) {
              self.models = models;
              return self;
           })
        },
        
        getModelSidebar: function(model_id) {
            var self = this;
            
            return rpc.query({
                model: 'archi.model',
                method: 'getModelSidebar',
                args: [[self.id]],
                kwargs: {'model_id':model_id}
            }).then(function (get_menu_list) {
                Object.assign(self.menu_list, get_menu_list)
                return self;
            }).catch(function (){
                
                console.log('Error!');
            });
        },
        get_diagrams: function(identifier) {
            var self = this;
            
            return rpc.query({
                model: 'archi.model',
                method: 'get_diagrams',
                args: [[self.id]],
                kwargs: {"identifier":identifier}
            }).then(function (get_image_childs) {
                Object.assign(self.diagrama, get_image_childs)
                self.diagrama = get_image_childs;
                return self;
            }).catch(function (){
                
                console.log('Error!');
            });
        },
        get_visualize: function(identifier,level) {
            var self = this;
            
            return rpc.query({
                model: 'archi.model',
                method: 'get_visualize',
                args: [[self.id]],
                kwargs: {"identifier":identifier,"level":level}
            }).then(function (get_visualize) {
                Object.assign(self.visualize, get_visualize)
                self.visualize = get_visualize;
                return self;
            }).catch(function (){
                console.log('Error!');
            });
        },
        
    });
  return archiModel;
});