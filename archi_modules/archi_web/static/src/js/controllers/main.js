odoo.define('archi_web.page_views', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');
    var menu = require('archi_web.menu');
    var archi_models = require('archi_web.archi_models');
    var visualize = require('archi_web.visualize');
    var diagrama = require('archi_web.diagrama');
    var Router = require('archi.router');
    var mainPage = require('archi_web.main_page');
    var model_list = require('archi_web.models_list');
    var statistics_page = require('archi_web.statistics_main_page')

var ArchiWebApp = Widget.extend({
    template:'archi_web.header_footer',
    
    xmlDependencies: ['/archi_web/static/src/xml/header_footer.xml'],
    events: {
        'click #sidebarCollapse': '_hideShowSidebar',
        'click .left-item': '_leftItemClick',
        'click area': '_areaDiagramaClick',
        'change #select_model': '_selectModel',
        'click #statistic_button': '_statisticsPage',
        'change #level_filters':'_visualizeLevel',
        'click #diagrama_sticker': '_scrollToDiagrama',
        'click #relations_sticker': '_scrollToRelations',
        'click #search_in_menu': '_searchInMenu',
        'change #search_input_menu': '_redrawMenu',
        'click #clear_search': '_clearSearch'
    },

    init: function(parent, options) {
        //this._super.apply(this,arguments);
        this._super(parent);
        var self = this;
        this.current_model;
        this.current_model_id;
        this.current_models;
        this.show_statistics = false;
        this.level = 1;
        this.item_id;
        this.search_active = false;
        this.main_page_present = false;
        Router.config({ mode: 'history', root: '/archi'});
        Router
        .add(/main/,function(){
                self._mainPage();
        })
        .add(/statistics/,function(){
            self._statisticsPage();
        }).listen();
    },
   
    start: function () {
        var self = this;
        var archi_model = new archi_models(self);  
        return this._super.apply(this,arguments).then( function() {
            
        return archi_model.getArchimateModels()}).then(
            function(current_model) {
                self.current_model = current_model.models[0];
                self.current_model_id = current_model.models[0].id;
                self.current_models = current_model;
                return archi_model.getModelSidebar(self.current_model.id)
            }
        ).then( function(result_menu) {
        var menuWidget = new menu(self, result_menu.menu_list,'');
        menuWidget.replace($('.navbar_widget'));
        self._mainPage();

        return result_menu
        
        }).then(function(result_menu) {
              var models_list = new model_list(self,self.current_models);
              models_list.replace($('.models_list'));
              Router.check();
              $('.model_name').text(self.current_model.name);
              self._firstElementModel(result_menu.menu_list.first_item_in_model);

              return result_menu
        }).then( function(result_menu) {
          self._firstDiagramaArea(result_menu.menu_list.first_visualize_item);
        })
        
        
    },

    _mainPage: function () {
        var self = this;
        
        var main_page = new mainPage(self);
        main_page.replace($('.main_page_widget'));
        // $('.main_page_widget').replaceWith(`
        //     <div class="main_page_widget" style="margin:0 auto; margin-left:0;">
        //     <img src="/archi_web/static/src/img/enterprise_architecture.jpeg" />
        //     </div>`)
            self.main_page_present = false;
        Router.navigate('/main')
           
    },
    _hideShowSidebar: function() {
        $('#sidebar').toggleClass('active');
    },
    _leftItemClick: function(ev) {
        var self = this;
        ev.preventDefault();
        var item_id = ev.currentTarget.id;
        self.item_id = item_id;
        if (!self.main_page_present) {
            var main_page = new mainPage(self);
            main_page.replace($('.main_page_widget'));
            self.main_page_present = true;
        }
        
        if (self.show_statistics) {

            self.show_statistics = false;
            self._mainPage();
            $('#statistic_button').removeClass('btn-success');
            $('#statistic_button').addClass('btn-primary');
            $('#statistic_button').text('Statistics');
           
            
        }

        // $('.main_page_diagrama_visualize').show();
        // $('.first_page_image').hide();
        var data = new archi_models(self);
        if (ev.currentTarget.attributes['data-toggle'] == undefined) {
        
            return data.get_diagrams(item_id).then( function (result_diagrama) {
                var diagramaWidget = new diagrama(self,result_diagrama.diagrama);
                if ($('#diagrama').attr('aria-expanded') != "true" && result_diagrama.diagrama.childs.length > 0) {
                  $('#diagrama').click();
                }
                if ( $('#diagrama').attr('aria-expanded') == "true"
                           && result_diagrama.diagrama.childs.length == 0) {
                           $('#diagrama').click();

                }
                if ( $('#diagrama').attr('aria-expanded') == "true"
                           && result_diagrama.diagrama.childs.length > 0
                           && ev.currentTarget.nodeName == "AEREA") {
                           $('#diagrama').click();

                }

                if ( $('#relations').attr('aria-expanded') == "false") {
                        $('#relations').click();
                   }

                if (result_diagrama.diagrama.childs.length > 0) {
                  document.querySelector("#diagrama").scrollIntoView({ behavior: 'smooth'});
                  $('#diagrama').show();
                  $('#diagrama_sticker').show();
                  $('#relations_sticker').show();
                } else {
                  $('#diagrama').hide();

                  $('#diagrama_sticker').hide();
                  $('#relations_sticker').hide();
                }

                diagramaWidget.replace($('.diagrama_widget'));


        }).then( function () {
                return data.get_visualize(item_id, self.level)
            }).then( function (result_visualize) {
                var visualizeWidget = new visualize(self,result_visualize.visualize);
                visualizeWidget.replace($('.visualize_widget'));
        }); } else {
            return data.get_visualize(item_id, self.level).then( function (result_visualize) {
            var visualizeWidget = new visualize(self,result_visualize.visualize);
            visualizeWidget.replace($('.visualize_widget'));
            $('#relations_header').text('');
              }).then( function () {
//              if ($('#diagrama').attr('aria-expanded') == 'true') {
//                $('#diagrama').click();
//              }
              if ($('#relations').attr('aria-expanded') == 'true') {
                  $('#relations').click();}
              });



                                                  
        }
        

    },
    _firstElementModel: function(identifier) {
        var self = this;
        var item_id = identifier;
        self.item_id = item_id;
        if (!self.main_page_present) {
            var main_page = new mainPage(self);
            main_page.replace($('.main_page_widget'));
            self.main_page_present = true;
        }

        if (self.show_statistics) {

            self.show_statistics = false;
            self._mainPage();
            $('#statistic_button').removeClass('btn-success');
            $('#statistic_button').addClass('btn-primary');
            $('#statistic_button').text('Statistics');


        }

        // $('.main_page_diagrama_visualize').show();
        // $('.first_page_image').hide();
        var data = new archi_models(self);


            return data.get_diagrams(item_id).then( function (result_diagrama) {
                var diagramaWidget = new diagrama(self,result_diagrama.diagrama);
                $('#diagrama').click();
                diagramaWidget.replace($('.diagrama_widget'));


        }).then( function () {
                return data.get_visualize(item_id, self.level)
            }).then( function (result_visualize) {
                var visualizeWidget = new visualize(self,result_visualize.visualize);
                visualizeWidget.replace($('.visualize_widget'));
        });




    },
    _areaDiagramaClick: function(ev) {
        ev.preventDefault();
        var self = this;
        var item_id = ev.currentTarget.id;
        self.item_id = item_id;
        var data = new archi_models(self);
        return data.get_visualize(item_id, self.level).then( function (result_visualize) {
                     var visualizeWidget = new visualize(self,result_visualize.visualize);
                     if ( $('#relations').attr('aria-expanded') != 'true')  {
                       $('#relations').click();
                     }

                     visualizeWidget.replace($('.visualize_widget'));
                    }).then( function() {
                      document.querySelector("#relations").scrollIntoView({ behavior: 'smooth'});
                    })
    },
    _firstDiagramaArea: function(visualize_identifier) {
        var self = this;
        var item_id = visualize_identifier;

       self.item_id = item_id;
        var data = new archi_models(self);
        return data.get_visualize(item_id, self.level).then( function (result_visualize) {
                     var visualizeWidget = new visualize(self,result_visualize.visualize);
                     visualizeWidget.replace($('.visualize_widget'));

                    });
    },
    _selectModel: function(ev) {
        ev.preventDefault();
        
        var self = this;
        var model_id = parseInt(ev.target.value);
        this.current_model_id = model_id;
        var selected_option = ev.target.selectedOptions[0].text; 
        var archi_model = new archi_models(self);
        return archi_model.getArchimateModels().then(
            function(current_model) {
                return archi_model.getModelSidebar(model_id)
            }
        ).then( function(result_menu) {
        var menuWidget = new menu(self, result_menu.menu_list, '');
        menuWidget.replace($('.navbar_widget'));
        self._firstElementModel(result_menu.menu_list.first_item_in_model);
        $('.model_name').text(selected_option);
        // $('.main_page_widget').replaceWith(`
        //     <div class="main_page_widget" style="margin:0 auto; margin-left:0;">
        //     <img src="/archi_web/static/src/img/enterprise_architecture.jpeg" />
        //     </div>`);
            self.main_page_present = false;
        });
        
    },
    _statisticsPage: function(ev) {
        ev.preventDefault();
        var self = this;

        if (self.show_statistics) {

            self.show_statistics = false;
            self._mainPage();
            $('#statistic_button').removeClass('btn-success');
            $('#statistic_button').addClass('btn-primary');
            $('#statistic_button').text('Statistics');
            $('.first_page_image').hide();
            
        } else {
            var statistics = new statistics_page(self, this.current_model_id);
                statistics.replace($('.main_page_widget'));
                self.show_statistics = true;
                $('#statistic_button').removeClass('btn-primary');
                $('#statistic_button').addClass('btn-success');
                $('#statistic_button').text('Back to model');
            
        }
       

    },
    _visualizeLevel: function(ev) {
        ev.preventDefault();
        var self = this;
        var level = parseInt(ev.currentTarget.value)
        self.level = level;
        var data = new archi_models(self);
        return data.get_visualize(self.item_id, level).then( function (result_visualize) {
            var visualizeWidget = new visualize(self,result_visualize.visualize);
            visualizeWidget.replace($('.visualize_widget'));
           });
    },
    _scrollToDiagrama: function(ev) {
       ev.preventDefault();
       document.querySelector("#diagrama").scrollIntoView({ behavior: 'smooth'});
     },
     _scrollToRelations: function(ev) {
       ev.preventDefault();
       if ($('#relations').attr('aria-expanded') != "true") {
          $('#relations').click();
          console.log('click');
          setTimeout(scrollAfterClick, 1000)
          function scrollAfterClick() {
          document.querySelector("#relations").scrollIntoView({ behavior: 'smooth'});
          console.log('scroll!!');
          }
       } else {
          document.querySelector("#relations").scrollIntoView({ behavior: 'smooth'});
       }



     },
     _searchInMenu: function(ev) {
     ev.preventDefault();
     var self = this;

     self.search_active = !self.search_active;
     if (self.search_active) {
      $('#search_input_menu').show();
      $('.navbar_widget').css('margin-top','3.5rem')
      $('#clear_search').show();
     } else {
      $('#search_input_menu').hide();
      $('.navbar_widget').css('margin-top','unset');
      $('#clear_search').hide();
     }

     },
     _redrawMenu: function(ev) {
        ev.preventDefault;
        $('navbar_widget').replaceWith('<div class="navbar_widget" />');
        var current_search_value = document.getElementById('search_input_menu').value;
        var self = this;
        var model_id = self.current_model_id;
        var archi_model = new archi_models(self);

        return archi_model.getModelSidebar(model_id)
            .then( function(result_menu) {
        var menuWidget = new menu(self, result_menu.menu_list, current_search_value);
        return menuWidget.replace($('.navbar_widget'))

        }).then(function() {
          $('.navbar_widget').css('margin-top','3.5rem');
        });
     },
     _clearSearch: function() {
       $('navbar_widget').replaceWith('<div class="navbar_widget" />');
       var current_search_value = "";
        var self = this;
        var model_id = self.current_model_id;
        var archi_model = new archi_models(self);

        return archi_model.getModelSidebar(model_id)
            .then( function(result_menu) {
        var menuWidget = new menu(self, result_menu.menu_list, current_search_value);
        return menuWidget.replace($('.navbar_widget'))

        }).then(function() {
          $('.navbar_widget').css('margin-top','3.5rem');
          $('#search_input_menu').val('')

        });
     }





});

var $elem = $('.o_archi_app');
var app = new ArchiWebApp(null);
app.appendTo($elem)
});