odoo.define('archi_web.statistics_charts_widget', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var ArchiStatisticsCharts = Widget.extend({
    template:'archi_web.statistics_charts_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/statistics/charts.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,chart_data) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.data = chart_data;
    },
   
    start: function () {
    
        var self = this;
        if (self.data.amounts.length != 0) {
            $( "#application_chart" ).replaceWith(`<canvas id="application_chart" />`);
            $('#property').text(self.data.property);

            var dynamicColors = function() {
                        var r = Math.floor(Math.random() * 255);
                        var g = Math.floor(Math.random() * 255);
                        var b = Math.floor(Math.random() * 255);
                        return "rgb(" + r + "," + g + "," + b + ")";
                }

            var background_colors = [];
            for (var i=0;i<self.data.amounts.length; i++) {
                background_colors.push(dynamicColors());
            }

            var color = Chart.helpers.color;
                var horizontalBarChartData = {
                    labels: self.data.dimentions,
                    datasets: [{
                        label: '',
                        backgroundColor: background_colors,
                        borderColor: background_colors,
                        borderWidth: 1,
                        data: self.data.amounts
                    },]
                };
                
                var ctx = document.getElementById('application_chart').getContext('2d');
                window.myHorizontalBar = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: horizontalBarChartData,
                    defaults: {scale: {ticks: { min: 0 },}},
                    options: {
                         maintainAspectRatio: true,
                        layout: {
                                padding: {
                                    left: 10
                                }
                            },
                        scales: {
                            xAxes: [{ticks: {
                                    beginAtZero: true,
                                    fontSize: 10
                                }}],
                        yAxes: [{

                            BarThickness: 12,  // number (pixels) or 'flex'
                            maxBarThickness: 28 // number (pixels)
                        },

                        ]},
                        // Elements options apply to all of the options unless overridden in a dataset
                        // In this case, we are setting the border of each horizontal bar to be 2px wide
                    elements: {rectangle: {borderWidth: 2,}},
                    responsive: true,
                    legend: {  display: false  },
                    title: {display: true,text: ''},

                    }
                });



                
        } else {

            this.show = false;
            $('.application_chart').hide();
            $( "#application_chart" ).replaceWith(`<canvas id="application_chart" />`);

        }
        
        return this._super.apply(this, arguments);
    }

    

});

return ArchiStatisticsCharts;
});