odoo.define('archi_web.statistics_table_widget', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var ArchiStatisticsTable = Widget.extend({
    template:'archi_web.statistics_table_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/statistics/table.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,statistic_table) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.statistic_table = statistic_table;
    },
   
    start: function () {
        var self = this;
        return this._super.apply(this,arguments)
    },

    

});

return ArchiStatisticsTable;
});