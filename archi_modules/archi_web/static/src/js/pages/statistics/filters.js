odoo.define('archi_web.statistics_filters_widget', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var ArchiStatisticsFilters = Widget.extend({
    template:'archi_web.statistics_filters_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/statistics/filters.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,filters) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.filters = filters;
    },
   
    start: function () {
        var self = this;
        return this._super.apply(this,arguments).then( function() {
            $('#statistic_filters').fastselect({placeholder:'Select properties for statistics'});
        })
    },

    

});

return ArchiStatisticsFilters;
});