odoo.define('archi_web.statistics_main_page', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');
    var statistics_filter = require('archi_web.statistics_filters_widget');
    var archi_models = require('archi_web.archi_models');
    var statistics_table = require('archi_web.statistics_table_widget');
    var chart_by_id = require('archi_web.statistics_charts_widget');

var ArchiStatisticsPage = Widget.extend({
    template:'archi_web.statistics_page_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/statistics/statistics.xml'],
    events: {
        'change #statistic_filters': '_showTable',
        'click .show_chart_by_id': '_showChartById'
    },

    init: function(parent,model_id) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.model_id = model_id;
    },
   
    start: function () {
        var self = this;
        return this._super.apply(this,arguments).then(function() {
            var archi_model = new archi_models(self);
            return archi_model.getStatisticsFilters(self.model_id).then( function(filters_result) {
                var filters = new statistics_filter(self,filters_result.statistic_filters);
                filters.replace($('.filters_widget'));

            })
        })
    },

    _showTable: function(ev) {
        var self = this;
        ev.preventDefault();
        var selected_filters = $('#statistic_filters').val();
        var archi_model = new archi_models(self);
        return archi_model.getStatisticTable(self.model_id, selected_filters).then( function(statistic_table) {
            var table = new statistics_table(self,statistic_table.statistic_table);
            table.replace($('.table_widget'));
            $( "#application_chart" ).replaceWith(`<canvas id="application_chart" />`);
            $('#property').text("");
        })
    },
    _showChartById: function(ev) {
        var self = this;
        ev.preventDefault();
        var id_property = parseInt(ev.currentTarget.attributes.id.value);
        var archi_model = new archi_models(self);
        return archi_model.getStatisticsChartDataById(self.model_id, id_property).then( function(chart_data) {
            var chart = new chart_by_id(self, chart_data.statistic_chart);
            chart.replace($('.charts_widget'));
        })

        
    }

    

});

return ArchiStatisticsPage;
});