odoo.define('archi_web.visualize', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');
    var visualize_data = require('archi_web.archi_models');
    // var visualize = require('archi_web.visualize');
    var properties = require('archi_web.properties');
    var archi_model = require('archi_web.archi_models');

var archiVisualize = Widget.extend({
    template:'archi_web.visualize_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/main/visualize.xml'],
    events: {
        'click .element_image': '_filterImages',
        'click .arrow-back': '_arrowBack'
    },

    init: function(parent,visualize) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.visualize = visualize;
        this.nodes = visualize.nodes;
        this.edges = visualize.edges; 
        this.level = parent.level;
        this.properties = visualize.properties
        this.element_documentation = visualize.element_documentation;
        this.image_filter = [];
        this.last_click_visualizer = false;
        this.history = [visualize.identifier];
        this.images = this._getImagesList(this.nodes);
        //this.nodes = [{'label': 'Test PF', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': '45b98f14-4dcc-4c95-a11c-e8a43cd68594'}, {'label': 'Unlock PF online', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': '830071ea-f9ea-45c8-a0ba-c8e59dddf9da'}, {'label': 'Mbx move Error handling', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': '41da3ed1-1a48-49e1-9e5c-bb292bbc2cc6'}, {'label': 'PF move Error handling', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': '8a774cd6-fe3b-4756-b6c4-72ac3c0d318a'}, {'label': 'PF Move', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55'}, {'label': 'Finalize PF migration', 'shape': 'image', 'image': '/archi_web/static/icons/businessprocess.png', 'id': '9fab6b50-deb1-458e-a9c2-312536a756a6'}, {'label': 'Business user', 'shape': 'image', 'image': '/archi_web/static/icons/businessrole.png', 'id': 'cad1069c-eb60-4733-bd47-ad6323c3c444'}];
        //this.edges =  [{'to': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'from': '830071ea-f9ea-45c8-a0ba-c8e59dddf9da', 'arrows': 'to'}, {'to': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'from': '41da3ed1-1a48-49e1-9e5c-bb292bbc2cc6', 'arrows': 'to'}, {'to': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'from': '8a774cd6-fe3b-4756-b6c4-72ac3c0d318a', 'arrows': 'to'}, {'to': '41da3ed1-1a48-49e1-9e5c-bb292bbc2cc6', 'from': '34e73705-3fd6-433e-ad5c-7af7cb78258f', 'arrows': 'to'}, {'to': '41da3ed1-1a48-49e1-9e5c-bb292bbc2cc6', 'from': 'ffa31646-76a5-470e-b2ac-b2aacb062834', 'arrows': 'to'}, {'to': '8a774cd6-fe3b-4756-b6c4-72ac3c0d318a', 'from': 'ffa31646-76a5-470e-b2ac-b2aacb062834', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': 'da216306-841c-4102-89b0-d49e14aeea41', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '032f1249-c29d-43a3-8b88-c4bfb4ab9759', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': 'c8ee5df6-74fe-41cd-8567-8e95d8b6761a', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '0e4bde72-27be-45a1-9461-df48825f51b7', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '49ce575d-7996-4316-af9f-c60901173782', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '9fab6b50-deb1-458e-a9c2-312536a756a6', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '41da3ed1-1a48-49e1-9e5c-bb292bbc2cc6', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': 'ffa31646-76a5-470e-b2ac-b2aacb062834', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '4b7a05e2-5b97-4761-9da9-a6f331836754', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '830071ea-f9ea-45c8-a0ba-c8e59dddf9da', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': '8a774cd6-fe3b-4756-b6c4-72ac3c0d318a', 'arrows': 'to'}, {'to': 'ea060ebe-9697-4ec7-b6ed-dd281f3bfe55', 'from': 'de09e8bd-448c-431d-95b8-f7088863e803', 'arrows': 'to'}, {'to': '9fab6b50-deb1-458e-a9c2-312536a756a6', 'from': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'arrows': 'to'}, {'to': 'cad1069c-eb60-4733-bd47-ad6323c3c444', 'from': 'a61254b1-c0f7-41c5-acc9-1515fcb02b59', 'arrows': 'to'}, {'to': 'cad1069c-eb60-4733-bd47-ad6323c3c444', 'from': '45b98f14-4dcc-4c95-a11c-e8a43cd68594', 'arrows': 'to'}];
    },
   
    start: function () {
        var self = this;
        var network = null;
        $('#level_filters').niceSelect();
        $('#level_filters').val(this.level);
        $('#level_filters').niceSelect('update');
        if (self.nodes.length > 0) {
          self._renderVisualize(self.nodes, self.edges, self.visualize.name_visualizer);
          self._propertiesForElement(self.visualize.identifier);
        }

        return this._super.apply(this,arguments)
    },
    _propertiesForElement: function(element_id) {
      var self = this;
      var data = new archi_model(self);
      data.getPropertiesByElementID(element_id).then(function(properties_list) {
        var properties_widget = new properties(self,properties_list);
        properties_widget.replace($('.properties_widget'));
      })


//      var table_properties = '';
//      var table_before = `
//          <div class="properties_widget">
//                  <div style="width: 22%;display: inline-block;position: absolute;left: 80%;left: 76%;">
//                <div>Element properties</div>
//                <table style="width:100%">
//                        <tr>
//                            <th style="text-align:center;">Name</th><th style="text-align:center;">Value</th>
//                        </tr>`
//
//      var table_after = `
//        </table>
//                    </div></div>
//      `
//      for (var i=0;i<properties_list.length;i++) {
//
//        table_properties = table_properties + `
//                            <tr>
//                                <td>${properties_list[i].p_name}</td>
//                                <td>${properties_list[i].value}</td>
//                            </tr>
//        `
//      }
//      $('.properties_widget').replaceWith(table_before+table_properties+table_after);
    },

    _renderVisualize: function (nodes,edges,visualizer) {
        var self = this;
        var network = null;
        var container = document.getElementById('mynetwork');
        self.images = self._getImagesList(nodes);
        $('#relations_header').text(visualizer);
        self._propertiesForElement(nodes[0]);

        var set_nodes = new vis.DataSet(self.nodes);
        var set_edges = new vis.DataSet(self.edges);
        var data = {
            nodes: set_nodes,
            edges: set_edges
        };

        var options = {
        physics: {
           enabled: false
         }
            // edges: {
            //     arrows: {
            //     to: {
            //         scaleFactor: 0.5
            //     }
            //     }
            // }
        };
        //network  handlers
        network = new vis.Network(container, data, options);

        network.on("click", function (params) {
            if (params.nodes.length>0){
                console.log(params);
                self._propertiesForElement(params.nodes[0]);
            }
        });
        network.on("doubleClick", function (params) {
            //location.href = params.nodes;
            console.log(params.nodes[0]);
            if (params.nodes.length != 0) {
                self._doubleClick(params.nodes[0]);
                self._propertiesForElement(params.nodes[0]);
            }
            
        });

        //self._replaceElement(qweb.render('archi_web.visualize_widget'), self);
    },

    _renderVisualizeRedraw: function (nodes,edges,name_visualizer) {
        var self = this;
        var network = null;
        self.nodes = nodes;
        self.edges = edges;
        self.images = self._getImagesList(nodes);
        $('#relations_header').text(name_visualizer);

        self._propertiesForElement(nodes[0].id);

        self._replaceElement(qweb.render('archi_web.visualize_widget', {widget:this}));
        $('#level_filters').niceSelect();
        $('#level_filters').val(this.level);
        $('#level_filters').niceSelect('update');
        var container = document.getElementById('mynetwork');
        var set_nodes = new vis.DataSet(nodes);
        var set_edges = new vis.DataSet(edges);
        var data = {
            nodes: set_nodes,
            edges: set_edges
        };

        var options = { width: '100%',physics: {
           enabled: false
         }};
        //network click handlers
        network = new vis.Network(container, data, options);
        network.on("click", function (params) {
            if (params.nodes.length>0){
                console.log(params);
                self._propertiesForElement(params.nodes[0]);
            }
        });
        network.on("doubleClick", function (params) {
            //location.href = params.nodes;
            console.log(params.nodes[0]);
            if (params.nodes.length != 0) {
                self._doubleClick(params.nodes[0]);
                self._propertiesForElement(params.nodes[0]);
            }
            
        });
        network.redraw();

        //self._replaceElement(qweb.render('archi_web.visualize_widget'), self);
    },
    _arrowBack: function() {
    var self = this;
    var current_identifier;
    if (this.history.length > 2) {
      current_identifier = this.history[this.history.length - 2];
      this.history.pop();
    } else if (this.history.length <= 2) {
      current_identifier = this.history[0];
      this.history.pop();
    }  else {
      current_identifier = this.history[0];
    }
    this.last_click_visualizer = false;

    return  self._doubleClickAction(current_identifier).then( function(){
              if (self.history.length != 1) {
                     $('.arrow-back').show();
                     console.log('arrow: ' + self.history)
              }

           });

    },
    _doubleClick: function(identifier){
     var self = this;
     this.last_click_visualizer = true;
     this.history.push(identifier);
     self._doubleClickAction(identifier).then( function() {
                    $('.arrow-back').show();
                    console.log('visualizer: '+self.history)
     });
    },

    _doubleClickAction: function (identifier) {

        var self = this;
        var identifier = identifier;
        $('#mynetwork').replaceWith(`<div id="mynetwork" ></div>`);
        var data = new visualize_data(self);
        $('#level_filters').niceSelect();
        $('#level_filters').val(this.level);
        $('#level_filters').niceSelect('update');
        this.image_filter = [];

        //self._replaceElement(qweb.render('archi_web.visualize_widget'), self);
        identifier,self.level
        return data.get_visualize(identifier,self.level).then( function (result_visualize) {
            self.images = self._getImagesList(result_visualize.visualize.nodes);
            self._renderVisualizeRedraw(result_visualize.visualize.nodes,
                                        result_visualize.visualize.edges,
                                        result_visualize.visualize.name_visualizer);
            //self._propertiesForElement(result_visualize.visualize.properties);
            
           });
    },

    _getImagesList: function(nodes) {
        var image_list = [];
        for (var i = 0; i < nodes.length; i++) {
           if ( !image_list.includes(nodes[i].image) ) {
               image_list.push(nodes[i].image);
                            
           }
        }

        return image_list
    },
    _filterImages: function(ev) {
        ev.preventDefault();
        var self = this;
        var nodes = [];
        var edges = [];
        var image_url = ev.currentTarget.attributes.src.value;

        if (!self.image_filter.includes(image_url)) 
        {
            self.image_filter.push(image_url);
            $(ev.currentTarget).css('background-color','white')
            image_url = "";
        } 
        if (self.image_filter.includes(image_url))  {
            for( var n = 0; n < self.image_filter.length; n++){ if ( self.image_filter[n] === image_url) { self.image_filter.splice(n, 1); }}
            $(ev.currentTarget).css('background-color','lightgrey')
        }

        var node_ids = self._changeIdFromNodes(self.nodes);
        for (var i = 0; i<self.nodes.length; i++) {
            if (node_ids.includes(self.nodes[i].id)) {
                nodes.push(self.nodes[i]);
            }
        }

        for (var j = 0; j<self.edges.length; j++) {
            if (node_ids.includes(self.edges[j].from) || node_ids.includes(self.edges[j].to)) {
                edges.push(self.edges[j]);
            }
        }

        $('#level_filters').niceSelect();
        $('#level_filters').val(this.level);
        $('#level_filters').niceSelect('update');
        //self._replaceElement(qweb.render('archi_web.visualize_widget'), self);
        return  self._renderVisualizeRedrawFilter(nodes, edges);
        
    },

    _changeIdFromNodes: function(nodes) {
        var self = this;
        var new_nodes = []
        for (var i = 0; i < nodes.length; i++) {
            if (!self.image_filter.includes(nodes[i].image)) {
                new_nodes.push(nodes[i].id);
            }
        }
        return new_nodes;
    },
    _renderVisualizeRedrawFilter: function (nodes,edges) {
        var self = this;
        var network = null;
        var container = document.getElementById('mynetwork');
        var data = {
            nodes: nodes,
            edges: edges
        };

        var options = {
            width: '100%',
            physics: {
           enabled: false
         },
            edges: {
                arrows: {
                to: {
                    scaleFactor: 0.5
                }
                }
            }
        };
        //network click handlers
        network = new vis.Network(container, data, options);
        network.on("click", function (params) {
            if (params.nodes.length>0){
                console.log(params);
                self._doubleClick(params.nodes[0]);
                self._propertiesForElement(params.nodes[0]);
            }
        });
        network.on("doubleClick", function (params) {
            //location.href = params.nodes;
            console.log(params.nodes[0]);
            if (params.nodes.length != 0) {
                self._doubleClick(params.nodes[0]);
                self._propertiesForElement(params.nodes[0]);
            }
            
        });
        network.redraw();

        //self._replaceElement(qweb.render('archi_web.visualize_widget'), self);
    },

    

});

return archiVisualize;
});