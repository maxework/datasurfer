odoo.define('archi_web.main_page', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var ArchiMainPage = Widget.extend({
    template:'archi_web.main_page_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/main/main_page_widget.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,menu_list) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
    },
   
    start: function () {
        var self = this;
        return this._super.apply(this,arguments)
    },

    

});

return ArchiMainPage;
});