odoo.define('archi_web.menu', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var ArchiMenu = Widget.extend({
    template:'archi_web.menu_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/main/menu_widget.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,menu_list,current_search) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.menu_list = menu_list.result;
        this.menu_data = [];
        this.current_search = current_search.toUpperCase();
        
    },
   
    start: function () {
        var current_search = this.current_search;
        var result_html = `<div class="navbar_widget" ><ul class="list-unstyled components mb-5">`;
        var result_childs_html = "";
        var result_childs;
        for (var i = 0; i< this.menu_list.length; i++) {
        //if (this.menu_list[i].name.toUpperCase().search(current_search) != -1 || current_search == '' ) {
           if (this.menu_list[i].childs == undefined) {
               result_html = result_html+`<li>
                                            <a href="#" class="left-item" id="${this.menu_list[i].identifier }">
                                             <span> ${this.menu_list[i].name}</span>

                                            </a>

                                          </li>`
           } else {
               result_childs= this.get_child_items(this.menu_list[i], 0);
                result_childs_html = result_childs.html;
               var count_childs = result_childs.count_childs
               result_html = result_html+`
                   <li>
                      <a href="${this.menu_list[i].href_identifier }" data-toggle="collapse" id="${this.menu_list[i].identifier}"
                      aria-expanded="false" class="dropdown-toggle left-item"><span>${this.menu_list[i].name}</span>

                       </a>
                        <ul class="collapse list-unstyled" id="id${this.menu_list[i].identifier}">
               `

               result_html = result_html+result_childs_html+'</ul></li>'
           }
        //}
        }
        result_html = result_html+'</ul></div>'
        $('.navbar_widget').replaceWith(result_html);
        return this._super.apply(this,arguments);
    },
    get_child_items: function (item, count_childs) {
      var self = this;
      var current_search = self.current_search;
      var result_html = "";
      var result_childs_html = "";
      var result_childs;
      var count_childs = count_childs;
      for (var i = 0; i<item.childs.length;i++) {
      if (item.childs[i].name.toUpperCase().search(current_search) != -1 || current_search == '' ) {
          if (item.childs[i].childs == undefined) {
            result_html = result_html+`<li>
            <a href="#" class="left-item" id="${item.childs[i].identifier }">
              ${item.childs[i].name}
            </a>
          </li>`
          count_childs += 1;
          } else {

            result_childs = this.get_child_items(item.childs[i], count_childs);
            result_childs_html = result_childs.html;
            count_childs += result_childs.count_childs+1;
            result_html = result_html+`
            <li>
               <a href="${item.childs[i].href_identifier }" data-toggle="collapse" id="${item.childs[i].identifier}"
               aria-expanded="false" class="dropdown-toggle left-item">${item.childs[i].name}
               </a>
                 <ul class="collapse list-unstyled" id="id${item.childs[i].identifier}">
        `

            result_html = result_html+result_childs_html+'</ul></li>'
            }
      }
      }
      return {html:result_html,count_childs:count_childs};
    }


    

});

return ArchiMenu;
});

//<span style="color:#af9595; padding-left:5px;">${count_childs}</span>
//<span style="color:#af9595; padding-left:5px;">0</span>
//<span style="color:#af9595; padding-left:5px;">${count_childs}</span>