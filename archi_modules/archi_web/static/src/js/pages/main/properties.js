odoo.define('archi_web.properties', function (require) {
    'use strict';


    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');


var archiProperties = Widget.extend({
    template:'archi_web.properties_widget',

    xmlDependencies: ['/archi_web/static/src/xml/pages/main/properties.xml'],


    init: function(parent,element) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.properties = element.properties;
        this.element_documentation = element.element_documentation;
        this.element_name = element.element_name;
    },

    start: function () {
        var self = this;
        return this._super.apply(this,arguments)
    },

    _renderProperties: function (properties) {
        var self = this;
        self._replaceElement(qweb.render('archi_web.properties_widget'), self);
    }
  });

return archiProperties;
});