odoo.define('archi_web.models_list', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var modelsList = Widget.extend({
    template:'archi_web.models_list_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/main/models_list_widget.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,models_list) {
        //this._super.apply(this,arguments);
        this._super.apply(this, arguments);
        this.models_list = models_list.models;
    },
   
    start: function () {
        var self = this;
            return this._super.apply(this,arguments)
                   .then(function () {
                    $('#select_model').niceSelect();
                   });
    },

    

});

return modelsList;
});