odoo.define('archi_web.diagrama', function (require) {
    'use strict';
    
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    require('web.Class');
    require('web.rpc');
    var qweb = core.qweb;
    var _t = core._t;
    require('web.dom_ready');

var archiDiagram = Widget.extend({
    template:'archi_web.diagrama_widget',
    
    xmlDependencies: ['/archi_web/static/src/xml/pages/main/diagram_widget.xml'],
    // events: {
    //     'click #sidebarCollapse': '_hideShowSidebar'
    // },

    init: function(parent,result_diagrama) {
        this._super.apply(this, arguments);
        this.diagrama = result_diagrama;
        if (result_diagrama.childs.length > 0) {
          $('#diagrama_name').text(result_diagrama.name);
        } else {
          $('#diagrama_name').text('');
        }

    },
   
    start: function () {
        var self = this;
        $('map').imageMapResize();
        return this._super.apply(this,arguments)
    },

    

});

return archiDiagram;
});