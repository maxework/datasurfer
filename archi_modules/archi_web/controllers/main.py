# -*- coding: utf-8 -*-
import base64
import logging
import requests

import werkzeug
import werkzeug.datastructures

from odoo import http, _
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.addons.web.controllers.main import serialize_exception
from odoo.http import request
from odoo.tools import mimetypes
from odoo.addons.bus.controllers.main import BusController
import json
import ast
import time
from werkzeug.utils import redirect
from datetime import datetime
from odoo.addons.web.controllers.main import login_and_redirect, set_cookie_and_redirect
from odoo.addons.web.controllers.main import Session
from odoo.addons.website.controllers.main import Website
from odoo.service import security


class CustomerPortalArchi(CustomerPortal):
    
    @http.route(['/archi','/archi/<path:route>'], type='http', auth="user", website=True)
    def web_pages(self, **kw):
        request.uid = request.session.uid
        return request.render("archi_web.archi_pages_wrap", {})
