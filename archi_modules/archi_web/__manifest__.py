{
    'name': "Archi web",
    'version': '1.1',
    'depends': [
        'base',
        'bus',
        'web',
        'website',
        'archi_model_parsing'
    ],
    'external_dependencies': {
        'binary': ['unoconv']
    },
    'author': "Stepanets Sergey",
    'category': 'Application',
    'description': """
       Module models of archi 
    """,
    'data': [
       'views/assets.xml'
    ]
}