# coding: utf-8
import base64
from odoo import fields, models, api



class archiModelWeb(models.Model):

    _inherit = 'archi.model'

    
    def getStatisticsChartDataById(self, model_id, property_id):
        get_element = self.env['element.properties.list'].sudo().search([('name','=',property_id)])
        get_properties = self.env['properties.list'].sudo().browse(property_id)
        amounts = []
        dimentions = []
        for item in get_element:
            amounts.append(item.value)
            dimentions.append(item.element_id.name)
        print(amounts, dimentions)
        return {'amounts':amounts,'dimentions':dimentions,'property':get_properties.name}



    def getStatisticTable(self,model_id, properties_list):
        header_list = []
        header_list_id = []
        table = []
        get_properties = self.env['properties.list'].sudo().search([('id','in',properties_list)])
        for item_header in get_properties:
            header_list.append({'name':item_header.name,'id':item_header.id,  'property_type':item_header.property_type})
            header_list_id.append(item_header.id)
        
        get_elements = self.env['element.properties.list'].sudo().search([('name','in',get_properties.mapped('id'))]).mapped('element_id')
        for item_element in get_elements:
            application_values_list = []
            for item_value in header_list_id:
                get_value = self.env['element.properties.list'].sudo().search([('name','=',item_value),('element_id','=',item_element.id)])
                for item_property_value in get_value:
                    application_values_list.append(item_property_value.value)
            table.append([item_element.name]+application_values_list)

        result_dict = {
            "header": header_list,
            "table": table
        }
        return result_dict


    def getStatisticsFilters(self,model_id):
        result_list = []
        get_all_elements_with_properties = self.env['archi.element'].sudo()\
            .search([('property_ids','!=', False),('model_id','=',model_id)]).mapped('id')
        if bool(get_all_elements_with_properties):
            get_property_key_value = self.env['element.properties.list'].sudo().\
                search([('element_id','in',get_all_elements_with_properties)]).mapped('name.id')
            get_properties_list = self.env['properties.list'].sudo().search([('id','in',get_property_key_value)])
            for item in get_properties_list:
                result_list.append({'name':item.name, 'id':item.id})
        return result_list
            



    def getArchimateModels(self):
        result_list = []
        get_all_models = self.env['archi.model'].sudo().search([])
        for item_model in get_all_models:
            result_list.append({'name':item_model.name, 'id':item_model.id})
        return result_list

    def getModelSidebar(self,model_id):
        
        result_list = []
        first_item_in_model = ""
        def get_menu_items(parent_folder):
            get_folders = self.env['archi.model.menu'].sudo().search([('parent_folder','=',parent_folder),
                                                                     ('model_id','=',model_id)])
            folders_list = []
            if get_folders:
                for item_folder in get_folders:
                    folder_dict = {
                        "name":item_folder.name,
                        "identifier":item_folder.identifier,
                        "folder_type":item_folder.folder_type.name or False,
                        "href_identifier":'#id'+item_folder.identifier,
                        }
                    
                    check_child_folders = self.env['archi.model.menu'].sudo().search([('parent_folder','=',item_folder.id),
                                                                     ('model_id','=',model_id)])
                    check_child_elements = self.env['archi.element'].sudo().search([('parent_folder','=',item_folder.id)])
                    
                    if check_child_folders or check_child_elements:
                        folder_dict.update({'childs':get_menu_items(item_folder.id)})
                    folders_list.append(folder_dict)
            get_elements = self.env['archi.element'].sudo().search([('parent_folder','=',parent_folder)])
            
            elements_list = []
            if get_elements:
                for item_element in get_elements:
                    element_dict = {
                        "name":item_element.name,
                        "identifier":item_element.element_identifier,
                        "element_type":item_element.element_type.name or False,
                        "href_identifier":'#id'+item_element.element_identifier,
                        }
                    elements_list.append(element_dict)
            return folders_list+elements_list

        result_list = get_menu_items(False)
        first_item_in_model = self.get_first_item_in_model(model_id)
        first_visualize_item = ""
        if bool(first_item_in_model):
            get_element = self.env['archi.element'].search([('element_identifier','=',first_item_in_model)])
            try:
                first_visualize_item = get_element.child_ids[0].archimate_element_id.element_identifier
            except:
                print('GET ELEMENT ERROR:',get_element)
        print(result_list)
        result_dict = {'result':result_list,
                       'first_item_in_model':self.get_first_item_in_model(model_id),
                       'first_visualize_item':first_visualize_item}
        return result_dict

    
    def get_first_item_in_model(self,model_id):
        
        def get_element_identifier(parent_folder):
            result_identifier = ""
            check_child_folders = self.env['archi.model.menu'].sudo().search([('model_id','=',model_id),
                                                                              ('parent_folder','=',parent_folder),
                                                                              ('name','=','Views')],limit=1)
            if check_child_folders:
                check_child_elements = self.env['archi.element'].sudo().search([('parent_folder','=',check_child_folders.id)])
                if check_child_elements:
                    for item_element in check_child_elements:
                        return item_element.element_identifier
                else:
                    get_subfolder_id = self.env['archi.model.menu'].sudo().search([('parent_folder','=',check_child_folders.id)])
                    if get_subfolder_id:
                        for item_folder in get_subfolder_id:
                            result_identifier = get_element_identifier(get_subfolder_id.id)
                            if bool(item_folder):
                                return result_identifier
            return result_identifier

        result_identifier = get_element_identifier(False)

        return result_identifier



    def get_diagrams(self,identifier):
        get_element = self.env['archi.element'].search([('element_identifier','=',identifier)])
        child_ids_list = []
        
        if get_element:
            
            for item_child in get_element.child_ids:
                diagrama_link = False
                if item_child.child_type == 'archimate:DiagramModelReference':
                    diagrama_link = True
                x = str(item_child.bounds_x)
                y = str(item_child.bounds_y)
                x1 = str(item_child.bounds_x1)
                y1 = str(item_child.bounds_y1)
                # x1 = str(item_child.bounds_x+item_child.bounds_width)
                # y1 = str(item_child.bounds_y+item_child.bounds_height)
                child_ids_list.append({
                    'element_identifier':item_child.archimate_element_id.element_identifier,
                    'coords':'%s,%s,%s,%s' % (x,y,x1,y1),
                    'diagrama_link':diagrama_link
                    })
            
            diagrama = {'name': get_element.name,
                        'image':get_element.diagrama,
                        'childs':child_ids_list}
            print('diagrama identifier:',identifier,'TEST CHILDS:',child_ids_list)
            return diagrama
        else:
            return {'image':get_element.diagrama,'childs':child_ids_list}


    
    

    def get_visualize(self, identifier, level):
        
        count = 0
        nodes = []
        edges = []
        all_relations = []
        all_elements = []
        identifier_list = []
        def get_relations(identifier):
            relations_ids = []
            all_identifiers = []
            get_sources = self.env['archi.element'].search([('source','=',identifier)])
            get_targets = self.env['archi.element'].search([('target','=',identifier)])
            # fill relations list for to from connections
            if get_sources:
                for item_source in get_sources:
                    relations_ids.append(item_source.element_identifier)
                    all_identifiers.append(item_source.source)
                    all_identifiers.append(item_source.target)
            if get_targets:
                for item_target in get_targets:
                    relations_ids.append(item_target.element_identifier)
                    all_identifiers.append(item_target.source)
                    all_identifiers.append(item_target.target)
            get_all_relations_identifier = self.env['archi.element'].search([('element_identifier','in',relations_ids)]).mapped('element_identifier')
            all_identifiers = list(set(all_identifiers))
            return get_all_relations_identifier,all_identifiers

        
        def get_level(identifier, identifier_list, count):
            
            all_relations, all_identifiers = get_relations(identifier)
            count += 1
            print(all_identifiers,identifier)
            all_identifiers = all_identifiers + identifier_list
            if count < level:
                for item_identifier in all_identifiers:
                    if item_identifier != identifier and item_identifier not in identifier_list:
                        all_relations = all_relations + get_level(item_identifier, all_identifiers, count)

            return all_relations
        
        all_relations = get_level(identifier, identifier_list, count)

        get_all_relations = self.env['archi.element'].search([('element_identifier','in',all_relations)])
        for relation_item in get_all_relations: 
            all_elements.append(relation_item.source)
            all_elements.append(relation_item.target)
            print(relation_item.element_type.name)
            dashes = {}
            arrows_type = {}
            if relation_item.element_type.name == 'FlowRelationship':
                arrows_type.update({
                    
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 12,
                        'src': "/archi_web/static/icons/arrows/arrow_full.svg"
                    },
                    
                })
                dashes.update({'dashes':True})
            if relation_item.element_type.name == 'ServingRelationship':
                arrows_type.update({
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src':"/archi_web/static/icons/arrows/arrow_line.svg"
                    }
                    })
            if relation_item.element_type.name == 'CompositionRelationship':
                arrows_type.update({
                    'from': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 20,
                        'imageHeight': 20,
                        'src':"/archi_web/static/icons/arrows/romb_full.svg"
                    },
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src': "/archi_web/static/icons/arrows/romb_empty.svg"
                    },})
            if relation_item.element_type.name == 'RealizationRelationship' or relation_item.element_type.name == 'RealisationRelationship':
                arrows_type.update({

                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src':"/archi_web/static/icons/arrows/arrow_empty.svg"
                    }
                    
                    })
                dashes.update({'dashes':[2,2]})

            if relation_item.element_type.name == 'SpecializationRelationship':
                arrows_type.update({

                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src':"/archi_web/static/icons/arrows/arrow_empty.svg"
                    }
                    
                    })
            if relation_item.element_type.name == 'AccessRelationship':
                arrows_type.update({
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src': "/archi_web/static/icons/arrows/arrow_line.svg"
                    },   })
                dashes.update({'dashes':[2,2]})
            if relation_item.element_type.name == 'TriggeringRelationship' or relation_item.element_type.name == 'UsedByRelationship':
                arrows_type.update({
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src': "/archi_web/static/icons/arrows/arrow_full.svg"
                    }, })
                
            if relation_item.element_type.name == 'AssignmentRelationship':
                arrows_type.update({
                    'from': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src': "/archi_web/static/icons/arrows/round_full.svg"
                    },
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 10,
                        'imageHeight': 10,
                        'src':
                        "/archi_web/static/icons/arrows/arrow_full.svg"
                    }
                    })
            if relation_item.element_type.name == 'AggregationRelationship':
                arrows_type.update({
                    'from': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 20,
                        'imageHeight': 20,
                        'src': "/archi_web/static/icons/arrows/romb_empty.svg"
                    },})
            if relation_item.element_type.name == 'InfluenceRelationship':
                arrows_type.update({
                    'to': {
                        'enabled': True,
                        'type': "image",
                        'imageWidth': 20,
                        'imageHeight': 20,
                        'src': "/archi_web/static/icons/arrows/arrow_plus.svg"
                    },})
                dashes.update({'dashes':True})

            node = {
                'to': relation_item.target, 
                'from': relation_item.source,
                'arrows': arrows_type }
                
            
            if bool(dashes):
                node.update(dashes)
            edges.append(node)
            


        # normilize list
        all_elements = list(set(all_elements))
        get_all_elements = self.env['archi.element'].search([('element_identifier','in',all_elements)])
        get_click_element = self.env['archi.element'].search([('element_identifier','=',identifier)])
        for item_element in get_all_elements:
            nodes.append({
                'label': item_element.name,
                'shape': 'image',
                'image': item_element.icon_path,
                'id': item_element.element_identifier
            })
        properties_list = self.get_properties_by_element_id(get_click_element.id)
        
        return {'nodes':nodes,
                'edges':edges,
                'identifier':identifier,
                'name_visualizer':get_click_element.name,
                'properties': properties_list.get('properties'),
                'element_documentation': properties_list.get("element_documentation")}


    def get_properties_by_element_id(self, element_id):
        """
        Get properties list by element id
        :param element_id:
        :return: [{'p_name':'Some property name','value':'value of property name'}]
        """
        if isinstance(element_id,int):
            current_element_id = element_id
        if isinstance(element_id, dict):
            current_element_id = self.env['archi.element'].search([('element_identifier','=',element_id.get('id'))]).id

        if isinstance(element_id,str):
            current_element_id = self.env['archi.element'].search([('element_identifier','=',element_id)]).id
        properties = self.env['element.properties.list'].sudo().search([('element_id', '=', current_element_id)])
        element_name = ''
        element_documentation = ''
        if current_element_id:
            element_object = self.env['archi.element'].browse(current_element_id)
            if element_object:
                element_name = element_object.name
                element_documentation = element_object.documentation
        result_properties_by_element = []
        for item_property in properties:
            result_properties_by_element.append({
                'p_name':item_property.name.name,
                'value':item_property.value
            })
        return {'properties':result_properties_by_element,
                'element_name': element_name,
                'element_documentation': element_documentation}
